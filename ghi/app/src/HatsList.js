import React from 'react';

class HatList extends React.Component {
    // set state
    state = {
        hats: [],
        locations: []
    }

    // get hats data from API
    async getHatList() {
        const response = await fetch("http://localhost:8090/api/hats")
        if(response.ok) {
            const data = await response.json();
            const hats = data.hats;
            this.setState({hats: hats});
            console.log(data)
        }
    }

    async componentDidMount() {
        this.getHatList();
    }

    async handleDelete(id) {
        const url = `http://localhost:8090/api/hats/${id}`
        await fetch(url, {method: "DELETE"})
        this.getHatList()
    }

    render() {
        return (
        <table className="table table-striped">
            <thead>
              <tr>
                <th>Name</th>
                <th>Fabric</th>
                <th>Color</th>
                <th>Location</th>
                <th>Picture</th>
                <th>Delete Hat</th>
              </tr>
            </thead>
            <tbody>
              {this.state.hats.map(hat => {
                return (
                  <tr key={hat.id}>
                    <td>{ hat.style_name }</td>
                    <td>{ hat.fabric}</td>
                    <td>{ hat.color }</td>
                    <td>{ hat.location.closet_name }</td>
                    <td><img src={ hat.picture_url } width="50" height="50"/></td>
                    <td><button type="button" className="btn btn-dark" onClick={() => this.handleDelete(hat.id)}>Delete</button></td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        );
    }
}

export default HatList;
