import {useEffect, useState} from 'react';

const HatsForm = () => {
    //set state for form fields
  const [ formData, setFormData ] = useState({
    fabric: "",
    style_name: '',
    color: '',
    picture_url: '',
    location: ''
  })

  // set state for locations drop-down
  const [ locations, setLocations ] = useState([])

  //useEffect() get data for locations drop-down
  useEffect(()=> {
    const loadData = async () => {
      const url = 'http://localhost:8100/api/locations/';
      const response = await fetch(url);

      if (response.ok) {
        const data = await response.json();
        setLocations(data.locations);
      } else {
        console.log("Error");
      }
    }

    loadData()
  }, [])

  const handleFormChange = (e) => {
    setFormData({
      ...formData,
      [e.target.name]: e.target.value
    })
  }

  // after clicking 'submit', send data to API
  const handleSubmit = async (e) => {
    e.preventDefault();

    const hatsUrl = 'http://localhost:8090/api/hats/';
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(formData),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(hatsUrl, fetchConfig);
    if (response.ok) {
      const newHats = await response.json();
      console.log(newHats);
      // after submit, clear form
      setFormData({
        fabric: "",
        style_name: '',
        color: '',
        picture_url: '',
        location: ''
      });
    }
  }

  return <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new hat</h1>
          <form onSubmit={handleSubmit} id="create-conference-form">

            <div className="form-floating mb-3">
              <input onChange={handleFormChange} value={formData.fabric} placeholder="Fabric" required type="text" name="fabric" id="fabric" className="form-control" />
              <label htmlFor="fabric">Fabric</label>
            </div>

            <div className="form-floating mb-3">
              <input onChange={handleFormChange} value={formData.style_name} placeholder="Style name" required type="text" name="style_name" id="style_name" className="form-control" />
              <label htmlFor="style_name">Style name</label>
            </div>

            <div className="form-floating mb-3">
              <input onChange={handleFormChange} value={formData.color} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
              <label htmlFor="color">Color</label>
            </div>

            <div className="form-floating mb-3">
              <input onChange={handleFormChange} value={formData.picture_url} placeholder="Picture URL" required type="url" name="picture_url" id="picture_url" className="form-control" />
              <label htmlFor="picture_url">Picture URL</label>
            </div>

            <div className="mb-3">
              <select onChange={handleFormChange} value={formData.location} required name="location" id="location" className="form-select">
                <option value="">Choose a location</option>
                {locations.map(location => {
                  return (
                    <option key={location.href} value={location.href}>{location.closet_name}</option>
                  )
                })}
              </select>
            </div>

            <button className="btn btn-dark">Create</button>
          </form>
        </div>
      </div>
    </div>
}

export default HatsForm;
